
方式1:基于浏览器  (访问时后跳到登录页面,登录成功后跳转到redirect_uri指定的地址) [GET]
http://localhost:8080/oauth/authorize?client_id=unity-client&redirect_uri=http%3a%2f%2flocalhost%3a8080%2funity%2fdashboard.htm&response_type=code&scope=read

响应的URL如:
http://localhost:8080/unity/dashboard.htm?code=zLl170

通过code换取access_token [GET]
http://localhost:8080/oauth/token?client_id=unity-client&client_secret=unity&grant_type=authorization_code&code=zLl170&redirect_uri=http%3a%2f%2flocalhost%3a8080%2funity%2fdashboard.htm


方式2:基于客户端  (注意参数中的username,password,对应用户的账号,密码) [GET]
http://localhost:8080/oauth/token?client_id=mobile-client&client_secret=mobile&grant_type=password&scope=read,write&username=admin&password=admin



获取access_token响应的数据如:
{"access_token":"3420d0e0-ed77-45e1-8370-2b55af0a62e8","token_type":"bearer","refresh_token":"b36f4978-a172-4aa8-af89-60f58abe3ba1","expires_in":43199,"scope":"read write"}


获取access_token后访问资源 [GET]
http://localhost:8080/unity/dashboard.htm?access_token=3420d0e0-ed77-45e1-8370-2b55af0a62e8



刷新access_token [GET]
http://localhost:8080/oauth/token?client_id=mobile-client&client_secret=mobile&grant_type=refresh_token&refresh_token=b36f4978-a172-4aa8-af89-60f58abe3ba1



------------------------------------------------------------------------------------------------
grant_type(授权方式)
1.authorization_code                      授权码模式(即先登录获取code,再获取token)
2.password                                     密码模式(将用户名,密码传过去,直接获取token)
3.refresh_token                            刷新token
4.implicit                                        简化模式(在redirect_uri 的Hash传递token; Auth客户端运行在浏览器中,如JS,Flash)
5.client_credentials                      客户端模式(无用户,用户向客户端注册,然后客户端以自己的名义向'服务端'获取资源)


scope
1.read
2.write
3.trust


